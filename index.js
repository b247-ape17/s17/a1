/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function UserInfo() {
		let fname  = prompt('Enter Your Name');
		let Age  = prompt('Enter Your Age');
		let Address  = prompt('Enter Your Address');

		console.log('Helow, ' + fname);
		console.log('You are ' + Age + ' years old.');
		console.log('You live in ' + Address);
	}
	UserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function FavoriteBand(){
		console.log('1.	The Beatles');
		console.log('2.	Metallica');
		console.log('3.	The Engles');
		console.log("4.	L'arc~en~Ciel");
		console.log('5.	Eraserheads');

	}
	FavoriteBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function topmovies() {
		let top1 = '1. The GodFather';
		let top2 = '2. The GodFather, Part II';
		let top3 = '3. Shawshank Redemption';
		let top4 = '4. To Kill A MockingBird';
		let top5 = '5. Psycho';


	function Tomatometer() {
		console.log(top1);
		console.log('Rotten Tomatoes Rating: 97%');
		console.log(top2);
		console.log('Rotten Tomatoes Rating: 96%');
		console.log(top3);
		console.log('Rotten Tomatoes Rating: 91%');
		console.log(top4);
		console.log('Rotten Tomatoes Rating: 93%');
		console.log(top5);
		console.log('Rotten Tomatoes Rating: 96%');

	}
  Tomatometer();
}
topmovies();
	

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printFriends(){
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();